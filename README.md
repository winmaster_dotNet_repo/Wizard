# Simple wizard project which enables user to fill questionnaire, and then to show typed data. 
## Each step is for one element only, user can go back to correct data, and on the end, decide to exit, or stay in application or to start again from scratch.
## On branch <b>master</b> you can find  very basic version of this project.
## More complicated version, which contains user input validation and unit tests is on <b>evaluation</b> branch.
## In this project, to improve my work, I used very useful  package - AdvancedWizard 4.0 by Stephen Bate, and NUnit package for Unit testing


# Screens:

![](screens/init.png)

![](screens/data.png)

![](screens/summary.png)



## Links

[AdvancedWizard 4.0](https://github.com/SteveBate/AdvancedWizard)

All icons / graphics are from [Iconfinder](https://www.iconfinder.com) as free