﻿namespace Wizard
{
    partial class Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Wizard));
            this.advancedWizard1 = new AdvancedWizardControl.Wizard.AdvancedWizard();
            this.advancedWizardPage6 = new AdvancedWizardControl.WizardPages.AdvancedWizardPage();
            this.textBoxSummary = new System.Windows.Forms.TextBox();
            this.advancedWizardPage5 = new AdvancedWizardControl.WizardPages.AdvancedWizardPage();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.labelPhone = new System.Windows.Forms.Label();
            this.advancedWizardPage4 = new AdvancedWizardControl.WizardPages.AdvancedWizardPage();
            this.groupBoxAddress = new System.Windows.Forms.GroupBox();
            this.textBoxHouseNumber = new System.Windows.Forms.TextBox();
            this.textBoxPostCode = new System.Windows.Forms.TextBox();
            this.textBoxStreet = new System.Windows.Forms.TextBox();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.labelHouseNr = new System.Windows.Forms.Label();
            this.labelPostCode = new System.Windows.Forms.Label();
            this.labelStreet = new System.Windows.Forms.Label();
            this.labelCity = new System.Windows.Forms.Label();
            this.advancedWizardPage3 = new AdvancedWizardControl.WizardPages.AdvancedWizardPage();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.labelSurname = new System.Windows.Forms.Label();
            this.advancedWizardPage2 = new AdvancedWizardControl.WizardPages.AdvancedWizardPage();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.advancedWizardPage1 = new AdvancedWizardControl.WizardPages.AdvancedWizardPage();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.advancedWizard1.SuspendLayout();
            this.advancedWizardPage6.SuspendLayout();
            this.advancedWizardPage5.SuspendLayout();
            this.advancedWizardPage4.SuspendLayout();
            this.groupBoxAddress.SuspendLayout();
            this.advancedWizardPage3.SuspendLayout();
            this.advancedWizardPage2.SuspendLayout();
            this.advancedWizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // advancedWizard1
            // 
            this.advancedWizard1.BackButtonEnabled = false;
            this.advancedWizard1.BackButtonText = "< Wstecz";
            this.advancedWizard1.ButtonLayout = AdvancedWizardControl.Enums.ButtonLayoutKind.Default;
            this.advancedWizard1.ButtonsVisible = true;
            this.advancedWizard1.CancelButtonText = "&Anuluj";
            this.advancedWizard1.Controls.Add(this.advancedWizardPage1);
            this.advancedWizard1.Controls.Add(this.advancedWizardPage6);
            this.advancedWizard1.Controls.Add(this.advancedWizardPage5);
            this.advancedWizard1.Controls.Add(this.advancedWizardPage4);
            this.advancedWizard1.Controls.Add(this.advancedWizardPage3);
            this.advancedWizard1.Controls.Add(this.advancedWizardPage2);
            this.advancedWizard1.CurrentPageIsFinishPage = false;
            this.advancedWizard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedWizard1.FinishButton = false;
            this.advancedWizard1.FinishButtonEnabled = true;
            this.advancedWizard1.FinishButtonText = "&Finish";
            this.advancedWizard1.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.advancedWizard1.HelpButton = false;
            this.advancedWizard1.HelpButtonText = "&Help";
            this.advancedWizard1.Location = new System.Drawing.Point(0, 0);
            this.advancedWizard1.Name = "advancedWizard1";
            this.advancedWizard1.NextButtonEnabled = true;
            this.advancedWizard1.NextButtonText = "Dalej >";
            this.advancedWizard1.ProcessKeys = false;
            this.advancedWizard1.Size = new System.Drawing.Size(663, 507);
            this.advancedWizard1.TabIndex = 0;
            this.advancedWizard1.TouchScreen = false;
            this.advancedWizard1.WizardPages.Add(this.advancedWizardPage1);
            this.advancedWizard1.WizardPages.Add(this.advancedWizardPage2);
            this.advancedWizard1.WizardPages.Add(this.advancedWizardPage3);
            this.advancedWizard1.WizardPages.Add(this.advancedWizardPage4);
            this.advancedWizard1.WizardPages.Add(this.advancedWizardPage5);
            this.advancedWizard1.WizardPages.Add(this.advancedWizardPage6);
            this.advancedWizard1.Cancel += new System.EventHandler(this.advancedWizard1_Cancel);
            this.advancedWizard1.Finish += new System.EventHandler(this.advancedWizard1_Finish);
            this.advancedWizard1.LastPage += new System.EventHandler(this.advancedWizard1_LastPage);
            // 
            // advancedWizardPage6
            // 
            this.advancedWizardPage6.Controls.Add(this.textBoxSummary);
            this.advancedWizardPage6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedWizardPage6.Header = true;
            this.advancedWizardPage6.HeaderBackgroundColor = System.Drawing.Color.White;
            this.advancedWizardPage6.HeaderFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.advancedWizardPage6.HeaderImage = ((System.Drawing.Image)(resources.GetObject("advancedWizardPage6.HeaderImage")));
            this.advancedWizardPage6.HeaderImageVisible = true;
            this.advancedWizardPage6.HeaderTitle = "Podsumowanie";
            this.advancedWizardPage6.Location = new System.Drawing.Point(0, 0);
            this.advancedWizardPage6.Name = "advancedWizardPage6";
            this.advancedWizardPage6.PreviousPage = 4;
            this.advancedWizardPage6.Size = new System.Drawing.Size(663, 467);
            this.advancedWizardPage6.SubTitle = "Twoje dane";
            this.advancedWizardPage6.SubTitleFont = new System.Drawing.Font("Tahoma", 8F);
            this.advancedWizardPage6.TabIndex = 6;
            // 
            // textBoxSummary
            // 
            this.textBoxSummary.AcceptsReturn = true;
            this.textBoxSummary.AcceptsTab = true;
            this.textBoxSummary.Enabled = false;
            this.textBoxSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSummary.Location = new System.Drawing.Point(31, 103);
            this.textBoxSummary.Multiline = true;
            this.textBoxSummary.Name = "textBoxSummary";
            this.textBoxSummary.ReadOnly = true;
            this.textBoxSummary.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxSummary.Size = new System.Drawing.Size(614, 361);
            this.textBoxSummary.TabIndex = 1;
            // 
            // advancedWizardPage5
            // 
            this.advancedWizardPage5.Controls.Add(this.textBoxPhone);
            this.advancedWizardPage5.Controls.Add(this.labelPhone);
            this.advancedWizardPage5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedWizardPage5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.advancedWizardPage5.Header = true;
            this.advancedWizardPage5.HeaderBackgroundColor = System.Drawing.Color.White;
            this.advancedWizardPage5.HeaderFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.advancedWizardPage5.HeaderImage = ((System.Drawing.Image)(resources.GetObject("advancedWizardPage5.HeaderImage")));
            this.advancedWizardPage5.HeaderImageVisible = true;
            this.advancedWizardPage5.HeaderTitle = "Krok 4";
            this.advancedWizardPage5.Location = new System.Drawing.Point(0, 0);
            this.advancedWizardPage5.Name = "advancedWizardPage5";
            this.advancedWizardPage5.PreviousPage = 3;
            this.advancedWizardPage5.Size = new System.Drawing.Size(663, 467);
            this.advancedWizardPage5.SubTitle = "Podaj swój telefon";
            this.advancedWizardPage5.SubTitleFont = new System.Drawing.Font("Tahoma", 8F);
            this.advancedWizardPage5.TabIndex = 5;
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(197, 120);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(328, 29);
            this.textBoxPhone.TabIndex = 2;
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPhone.Location = new System.Drawing.Point(44, 125);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(74, 24);
            this.labelPhone.TabIndex = 1;
            this.labelPhone.Text = "Telefon";
            // 
            // advancedWizardPage4
            // 
            this.advancedWizardPage4.Controls.Add(this.groupBoxAddress);
            this.advancedWizardPage4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedWizardPage4.Header = true;
            this.advancedWizardPage4.HeaderBackgroundColor = System.Drawing.Color.White;
            this.advancedWizardPage4.HeaderFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.advancedWizardPage4.HeaderImage = ((System.Drawing.Image)(resources.GetObject("advancedWizardPage4.HeaderImage")));
            this.advancedWizardPage4.HeaderImageVisible = true;
            this.advancedWizardPage4.HeaderTitle = "Krok 3";
            this.advancedWizardPage4.Location = new System.Drawing.Point(0, 0);
            this.advancedWizardPage4.Name = "advancedWizardPage4";
            this.advancedWizardPage4.PreviousPage = 2;
            this.advancedWizardPage4.Size = new System.Drawing.Size(663, 467);
            this.advancedWizardPage4.SubTitle = "Podaj swój adres zamieszkania";
            this.advancedWizardPage4.SubTitleFont = new System.Drawing.Font("Tahoma", 8F);
            this.advancedWizardPage4.TabIndex = 4;
            // 
            // groupBoxAddress
            // 
            this.groupBoxAddress.Controls.Add(this.textBoxHouseNumber);
            this.groupBoxAddress.Controls.Add(this.textBoxPostCode);
            this.groupBoxAddress.Controls.Add(this.textBoxStreet);
            this.groupBoxAddress.Controls.Add(this.textBoxCity);
            this.groupBoxAddress.Controls.Add(this.labelHouseNr);
            this.groupBoxAddress.Controls.Add(this.labelPostCode);
            this.groupBoxAddress.Controls.Add(this.labelStreet);
            this.groupBoxAddress.Controls.Add(this.labelCity);
            this.groupBoxAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxAddress.Location = new System.Drawing.Point(31, 103);
            this.groupBoxAddress.Name = "groupBoxAddress";
            this.groupBoxAddress.Size = new System.Drawing.Size(620, 230);
            this.groupBoxAddress.TabIndex = 1;
            this.groupBoxAddress.TabStop = false;
            this.groupBoxAddress.Text = "Adres zamieszkania";
            // 
            // textBoxHouseNumber
            // 
            this.textBoxHouseNumber.Location = new System.Drawing.Point(428, 107);
            this.textBoxHouseNumber.Name = "textBoxHouseNumber";
            this.textBoxHouseNumber.Size = new System.Drawing.Size(92, 29);
            this.textBoxHouseNumber.TabIndex = 7;
            // 
            // textBoxPostCode
            // 
            this.textBoxPostCode.Location = new System.Drawing.Point(184, 173);
            this.textBoxPostCode.Name = "textBoxPostCode";
            this.textBoxPostCode.Size = new System.Drawing.Size(154, 29);
            this.textBoxPostCode.TabIndex = 6;
            // 
            // textBoxStreet
            // 
            this.textBoxStreet.Location = new System.Drawing.Point(184, 107);
            this.textBoxStreet.Name = "textBoxStreet";
            this.textBoxStreet.Size = new System.Drawing.Size(154, 29);
            this.textBoxStreet.TabIndex = 5;
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(184, 41);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(154, 29);
            this.textBoxCity.TabIndex = 4;
            // 
            // labelHouseNr
            // 
            this.labelHouseNr.AutoSize = true;
            this.labelHouseNr.Location = new System.Drawing.Point(392, 107);
            this.labelHouseNr.Name = "labelHouseNr";
            this.labelHouseNr.Size = new System.Drawing.Size(30, 24);
            this.labelHouseNr.TabIndex = 3;
            this.labelHouseNr.Text = "Nr";
            // 
            // labelPostCode
            // 
            this.labelPostCode.AutoSize = true;
            this.labelPostCode.Location = new System.Drawing.Point(13, 174);
            this.labelPostCode.Name = "labelPostCode";
            this.labelPostCode.Size = new System.Drawing.Size(128, 24);
            this.labelPostCode.TabIndex = 2;
            this.labelPostCode.Text = "Kod pocztowy";
            // 
            // labelStreet
            // 
            this.labelStreet.AutoSize = true;
            this.labelStreet.Location = new System.Drawing.Point(13, 110);
            this.labelStreet.Name = "labelStreet";
            this.labelStreet.Size = new System.Drawing.Size(51, 24);
            this.labelStreet.TabIndex = 1;
            this.labelStreet.Text = "Ulica";
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Location = new System.Drawing.Point(13, 46);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(64, 24);
            this.labelCity.TabIndex = 0;
            this.labelCity.Text = "Miasto";
            // 
            // advancedWizardPage3
            // 
            this.advancedWizardPage3.Controls.Add(this.textBoxSurname);
            this.advancedWizardPage3.Controls.Add(this.labelSurname);
            this.advancedWizardPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedWizardPage3.Header = true;
            this.advancedWizardPage3.HeaderBackgroundColor = System.Drawing.Color.White;
            this.advancedWizardPage3.HeaderFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.advancedWizardPage3.HeaderImage = ((System.Drawing.Image)(resources.GetObject("advancedWizardPage3.HeaderImage")));
            this.advancedWizardPage3.HeaderImageVisible = true;
            this.advancedWizardPage3.HeaderTitle = "Krok 2";
            this.advancedWizardPage3.Location = new System.Drawing.Point(0, 0);
            this.advancedWizardPage3.Name = "advancedWizardPage3";
            this.advancedWizardPage3.PreviousPage = 1;
            this.advancedWizardPage3.Size = new System.Drawing.Size(663, 467);
            this.advancedWizardPage3.SubTitle = "Podaj swoje nazwisko";
            this.advancedWizardPage3.SubTitleFont = new System.Drawing.Font("Tahoma", 8F);
            this.advancedWizardPage3.TabIndex = 3;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSurname.Location = new System.Drawing.Point(197, 120);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(328, 29);
            this.textBoxSurname.TabIndex = 2;
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSurname.Location = new System.Drawing.Point(44, 125);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(90, 24);
            this.labelSurname.TabIndex = 1;
            this.labelSurname.Text = "Nazwisko";
            // 
            // advancedWizardPage2
            // 
            this.advancedWizardPage2.Controls.Add(this.textBoxName);
            this.advancedWizardPage2.Controls.Add(this.labelName);
            this.advancedWizardPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedWizardPage2.Header = true;
            this.advancedWizardPage2.HeaderBackgroundColor = System.Drawing.Color.White;
            this.advancedWizardPage2.HeaderFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.advancedWizardPage2.HeaderImage = ((System.Drawing.Image)(resources.GetObject("advancedWizardPage2.HeaderImage")));
            this.advancedWizardPage2.HeaderImageVisible = true;
            this.advancedWizardPage2.HeaderTitle = "Krok 1";
            this.advancedWizardPage2.Location = new System.Drawing.Point(0, 0);
            this.advancedWizardPage2.Name = "advancedWizardPage2";
            this.advancedWizardPage2.PreviousPage = 0;
            this.advancedWizardPage2.Size = new System.Drawing.Size(663, 467);
            this.advancedWizardPage2.SubTitle = "Podaj swoje imię";
            this.advancedWizardPage2.SubTitleFont = new System.Drawing.Font("Tahoma", 8F);
            this.advancedWizardPage2.TabIndex = 2;
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxName.Location = new System.Drawing.Point(197, 120);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(328, 29);
            this.textBoxName.TabIndex = 2;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(44, 125);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(45, 24);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Imię";
            // 
            // advancedWizardPage1
            // 
            this.advancedWizardPage1.Controls.Add(this.pictureBoxImage);
            this.advancedWizardPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedWizardPage1.Header = true;
            this.advancedWizardPage1.HeaderBackgroundColor = System.Drawing.Color.White;
            this.advancedWizardPage1.HeaderFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.advancedWizardPage1.HeaderImage = ((System.Drawing.Image)(resources.GetObject("advancedWizardPage1.HeaderImage")));
            this.advancedWizardPage1.HeaderImageVisible = true;
            this.advancedWizardPage1.HeaderTitle = "Witaj w Kreatorze";
            this.advancedWizardPage1.Location = new System.Drawing.Point(0, 0);
            this.advancedWizardPage1.Name = "advancedWizardPage1";
            this.advancedWizardPage1.PreviousPage = 0;
            this.advancedWizardPage1.Size = new System.Drawing.Size(663, 467);
            this.advancedWizardPage1.SubTitle = "Kreator przeprowadzi Cię krok po kroku przez proces wypełniania formularza";
            this.advancedWizardPage1.SubTitleFont = new System.Drawing.Font("Tahoma", 8F);
            this.advancedWizardPage1.TabIndex = 1;
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxImage.Image = global::Wizard.Properties.Resources.wizard_start;
            this.pictureBoxImage.Location = new System.Drawing.Point(167, 103);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(286, 295);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxImage.TabIndex = 1;
            this.pictureBoxImage.TabStop = false;
            // 
            // Wizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 507);
            this.Controls.Add(this.advancedWizard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Wizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wizard";
            this.advancedWizard1.ResumeLayout(false);
            this.advancedWizardPage6.ResumeLayout(false);
            this.advancedWizardPage6.PerformLayout();
            this.advancedWizardPage5.ResumeLayout(false);
            this.advancedWizardPage5.PerformLayout();
            this.advancedWizardPage4.ResumeLayout(false);
            this.groupBoxAddress.ResumeLayout(false);
            this.groupBoxAddress.PerformLayout();
            this.advancedWizardPage3.ResumeLayout(false);
            this.advancedWizardPage3.PerformLayout();
            this.advancedWizardPage2.ResumeLayout(false);
            this.advancedWizardPage2.PerformLayout();
            this.advancedWizardPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AdvancedWizardControl.Wizard.AdvancedWizard advancedWizard1;
        private AdvancedWizardControl.WizardPages.AdvancedWizardPage advancedWizardPage6;
        private AdvancedWizardControl.WizardPages.AdvancedWizardPage advancedWizardPage5;
        private AdvancedWizardControl.WizardPages.AdvancedWizardPage advancedWizardPage4;
        private AdvancedWizardControl.WizardPages.AdvancedWizardPage advancedWizardPage3;
        private AdvancedWizardControl.WizardPages.AdvancedWizardPage advancedWizardPage2;
        private AdvancedWizardControl.WizardPages.AdvancedWizardPage advancedWizardPage1;
        private System.Windows.Forms.TextBox textBoxSummary;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.GroupBox groupBoxAddress;
        private System.Windows.Forms.TextBox textBoxHouseNumber;
        private System.Windows.Forms.TextBox textBoxPostCode;
        private System.Windows.Forms.TextBox textBoxStreet;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.Label labelHouseNr;
        private System.Windows.Forms.Label labelPostCode;
        private System.Windows.Forms.Label labelStreet;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.PictureBox pictureBoxImage;
    }
}

