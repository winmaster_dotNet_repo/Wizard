﻿using System;
using System.Windows.Forms;
using Wizard.Resources;

namespace Wizard
{
    public partial class Wizard : Form
    {
        public Wizard()
        {
            InitializeComponent();
        }

        private void advancedWizard1_LastPage(object sender, EventArgs e)
        {
            textBoxSummary.Text = "";
            textBoxSummary.Text = labelName.Text + "\t\t" + textBoxName.Text + "\r\n"
                                  + labelSurname.Text + "\t\t" + textBoxSurname.Text + "\r\n"
                                  + labelCity.Text + "\t\t" + textBoxCity.Text + "\r\n"
                                  + labelStreet.Text + "\t\t" + textBoxStreet.Text + "\t" + textBoxHouseNumber.Text + "\r\n"
                                  + labelPostCode.Text + "\t" + textBoxPostCode.Text + "\r\n"
                                  + labelPhone.Text + "\t\t" + textBoxPhone.Text; 
        }

        private void advancedWizard1_Finish(object sender, EventArgs e)
        {
            var questionResut = showMessageBox(Messages.FormEndQuestion, Messages.FormEnd);

            if (questionResut == DialogResult.No)
            {
                questionResut = showMessageBox(Messages.FormClearQuestion, Messages.FormClear);

                if (questionResut == DialogResult.Yes)
                {
                    textBoxCity.Text = "";
                    textBoxHouseNumber.Text = "";
                    textBoxName.Text = "";
                    textBoxPhone.Text = "";
                    textBoxPhone.Text = "";
                    textBoxPostCode.Text = "";
                    textBoxStreet.Text = "";
                    textBoxSummary.Text = "";
                    textBoxSurname.Text = "";

                    advancedWizard1.GoToPage(0);
                }
            }
            else
            {
                Application.Exit();
            }
        }

        private void advancedWizard1_Cancel(object sender, EventArgs e)
        {
            var questionResult = showMessageBox(Messages.FormCancelQuestion, Messages.FormCancel);

            if(questionResult == DialogResult.Yes) Application.Exit();
        }

        private DialogResult showMessageBox(string message, string caption)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            return MessageBox.Show(message, caption, buttons);
        }
    }
}
